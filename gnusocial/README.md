# GNU Social
Based on https://github.com/Enrico204/docker-gnusocial

This image contains a php7-fpm service and nginx. It contains the
nightly version of GS. It also has queuedaemons support, you only have to
activate in the GS config file as usual and it will be activated automatically
at start.

The main `Dockerfile` contains a vanilla GNU social installation, but the
`Dockerfile.daemons` doesn't. It contains the plugins and theme that I use on
[https://social.daemons.it](my instance).

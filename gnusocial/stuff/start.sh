#!/bin/sh

config_file="/var/www/html/config.php"
if [ -e $config_file ]
then
    startdaemons=$(grep "config\['queue'\]\['enabled'\]" $config_file | grep -i true)
    if [ -n "$startdaemons" ]
    then
        /bin/sh /var/www/html/scripts/startdaemons.sh
    fi
    rm /var/www/html/install.php
fi

nginx -c /etc/nginx/nginx.conf -g "daemon on;"
/usr/local/sbin/php-fpm

# Jira Cli
Simple [jira CLI](https://jiracli.com/).

Usage:

``` bash
touch $HOME/.jira-cl.json
docker run -ti -v $HOME/.jira-cl.json:/root/.jira-cl.json registry.daemons.it/jira
```

# linkcheck

A simple broken link checker. It's pretty fast and only returns the broken links:

``` bash
docker run -ti registry.daemons.it/linkcheck http://whatever
```

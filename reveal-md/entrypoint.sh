#!/bin/bash

for file in `ls files`
do
    ln -sf files/$file $file
done

if [[ -e files/presentation.css ]];then
    css="--css files/presentation.css"
fi

if [[ -e files/preproc.js ]]; then
    preproc="--preprocessor files/preproc.js"
fi

reveal-md files -w $css $preproc

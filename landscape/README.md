# Landscape

A tool to render `terraform plan` output:

``` bash
docker run -ti registry.daemons.it/landscape
```

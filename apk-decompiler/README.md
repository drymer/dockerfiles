<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgheadline1">1. Building Instructions</a></li>
<li><a href="#orgheadline2">2. Run with&#x2026;</a></li>
<li><a href="#orgheadline3">3. Start ssh server</a></li>
<li><a href="#orgheadline4">4. See docker's ip</a></li>
<li><a href="#orgheadline5">5. Send the apk to the docker container</a></li>
<li><a href="#orgheadline6">6. Decompile the apk</a></li>
<li><a href="#orgheadline7">7. Go to /root/src</a></li>
</ul>
</div>
</div>

# Building Instructions<a id="orgheadline1"></a>

    docker build -t apk-decompiler .

# Run with&#x2026;<a id="orgheadline2"></a>

    docker -t -i apk-decompiler

# Start ssh server<a id="orgheadline3"></a>

    service ssh start

# See docker's ip<a id="orgheadline4"></a>

    ifconfig eth0

# Send the apk to the docker container<a id="orgheadline5"></a>

    scp one.apk tmp@$IP:/home/tmp/

# Decompile the apk<a id="orgheadline6"></a>

    cd /home/tmp/
    automatic-decompile one.apk

# Go to /root/src<a id="orgheadline7"></a>

    cd /root/src

Profit.

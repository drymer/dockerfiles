# Tox
Docker image for python testing. It has installed tox and the next python
versions with pyenv:

- python2.7
- python3.4
- python3.5
- python3.6
- python3.7

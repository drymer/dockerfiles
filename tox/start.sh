#!/bin/bash

source ~/.bash_profile
eval "$(pyenv init -)"
pyenv local 2.7.15 3.4.8 3.5.5 3.6.6 3.7.0

eval "$@"

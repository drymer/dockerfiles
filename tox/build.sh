#!/bin/sh

docker build -f tox/Dockerfile -t "$WRITE_REGISTRY/tox:latest" tox/
docker build -f tox/Dockerfile_root -t "$WRITE_REGISTRY/tox:root" tox/

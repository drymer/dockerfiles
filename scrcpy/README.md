# scrcpy

Visualize your android phone in your computer. More information
[https://daemons.it/posts/visualizar-y-controlar-un-android-desde-el-ordenador/](here).
You may use the next alias:

```bash
alias scrcpy="docker run -ti --privileged -v /dev/bus/usb:/dev/bus/usb -v ~/.android:/root/.android -v /tmp/.X12-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -p 5037:5037 -v $HOME/.Xauthority:/root/.Xauthority --net=host scrcpy"
```

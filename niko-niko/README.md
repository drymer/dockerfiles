# Niko-niko calendar
A calendar to see the feelings of one or more people.

## Configuration
Copy the sqlite database:
``` bash
docker run -ti --name niko-niko registry.daemons.it/niko-niko sleep 1
docker cp niko-niko:/niko-niko/feelings.sqlite .
```

Run the webserver:

``` bash
docker run -ti -p 8080:8080 -v `pwd`/feelings.sqlite:/niko-niko/feelings.sqlite registry.daemons.it/niko-niko
```

## Use
The application supports multiple users. You can use it attacking to
`http://whatever:8080/?category=name`. You can visualiza the data at
`http://whatever:8080/results` or `http://whatever:8080/show_avg`. You may
filter using the `category` parameter.

# Reveal.js

With this image you can use Reveal.js without installing npm, which is always
nice. This image will expect to find your reveal files at `/revealjs/files`.
You may use this image with the next function:

``` bash
reveal () {
	async () {
		sleep 3 && xdg-open http:localhost:8000
	}
	async &|
	docker run -ti --name registry.daemons.it/revealjs --rm -v `pwd`:/revealjs/files/ -p 8000:8000 revealjs
}
```

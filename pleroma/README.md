# Pleroma
Forked from
[Angristan](https://github.com/Angristan/dockerfiles/blob/master/pleroma/Dockerfile).
It uses the `develop` branch since pleroma doesn't have a release system yet.

## Production use
In first use, execute:

``` bash
docker run -ti registry.daemons.it/pleroma sh
unset MIX_ENV
mix generate_config
```

The last order will ask some questions. Ask them and it will create a file
called `/pleroma/config/generated_config.exs`. Copy it locally, modify it and in
the next run, mount it:

``` bash
docker run -ti -v $PATH_TO_FILE/pleroma.secret.exs:/pleroma/config/prod.secret.exs registry.daemons.it/pleroma
```

## Testing use
To test in in a non production environment, you may use this `prod.secret.exs`
file:

``` text
use Mix.Config

config :pleroma, Pleroma.Web.Endpoint,
   url: [host: "localhost", scheme: "https", port: 443],
   secret_key_base: "5iqUT1buQk+120310230s00200s0as0d02020101"

config :pleroma, :instance,
  name: "Localhost",
  email: "nope@nope.com",
  limit: 5000,
  registrations_open: true,
  dedupe_media: false

# Configure your database
config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "pleroma",
  password: "pleroma",
  database: "pleroma",
  hostname: "postgres",
  pool_size: 10
```

Copy the previous file in `/tmp/pleroma.secret.exs` and execute:

``` bash
docker network create -d bridge pleroma
docker run --name postgres --rm -d -e POSTGRES_PASSWORD="pleroma" -e POSTGRES_USER=pleroma -e POSTGRES_DB=pleroma --net pleroma "postgres:9.6"
docker run --name pleroma --rm -d -v /tmp/pleroma.secret.exs:/pleroma/config/prod.secret.exs -p 4000:4000 --net pleroma registry.daemons.it/pleroma
```

It will take like a minute to pleroma to start. To clean when finished:

``` bash
docker stop postgres
docker stop pleroma
docker network rm pleroma
```

